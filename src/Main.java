import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration()
                .addAnnotatedClass(Employee.class)
                .addAnnotatedClass(Project.class)
                .configure();

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();

        // Create some test data
        Employee employee1 = new Employee("ABC");
        Employee employee2 = new Employee("DEF");
        Project project1 = new Project("Project A");
        Project project2 = new Project("Project B");
        employee1.getProjects().add(project1);
        employee2.getProjects().add(project1);
        employee2.getProjects().add(project2);
        session.save(employee1);
        session.save(employee2);
        session.save(project1);
        session.save(project2);

        // Fetch projects by employee ID
        EmployeeDao employeeDao = new EmployeeDao(sessionFactory);
        List<Employee> employees = employeeDao.getByProjectId(project1.getId());
        System.out.println("Employees working on Project A:");
        for (Employee employee : employees) {
            System.out.println(employee.getName());
        }

        // Fetch employees by project ID
        ProjectDao projectDao = new ProjectDao(sessionFactory);
        List<Project> projects = projectDao.getByEmployeeId(employee2.getId());
        System.out.println("Projects worked on by Jane Smith:");
        for (Project project : projects) {
            System.out.println(project.getName());
        }

        session.getTransaction().commit();
        sessionFactory.close();
    }
}
