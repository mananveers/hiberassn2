import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


public class EmployeeDao {

    private SessionFactory sessionFactory;

    public EmployeeDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Employee employee) {
        Session session = sessionFactory.getCurrentSession();
        session.save(employee);
    }

    public List<Employee> getByProjectId(int projectId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Employee> query = session.createQuery("SELECT e FROM Employee e JOIN e.projects p WHERE p.id = :projectId", Employee.class);
        query.setParameter("projectId", projectId);
        return query.getResultList();
    }
}
