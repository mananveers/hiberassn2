import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;


public class ProjectDao {

    private SessionFactory sessionFactory;

    public ProjectDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Project project) {
        Session session = sessionFactory.getCurrentSession();
        session.save(project);
    }

    public List<Project> getByEmployeeId(int employeeId) {
        Session session = sessionFactory.getCurrentSession();
        Query<Project> query = session.createQuery("SELECT p FROM Project p JOIN p.employees e WHERE e.id = :employeeId", Project.class);
        query.setParameter("employeeId", employeeId);
        return query.getResultList();
    }
}
